@extends('admin.layouts.app', ['active' => $var['title']])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $var['title'] }}</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ $var['title'] }}</a></li>
            <li class="breadcrumb-item active">Show</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="float-right mb-3">
        <a href="{{ route('user.edit', $data->id) }}"><button class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit {{ $var['title'] }}</button></a>
        {{-- <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus {{ $var['title'] }}</button> --}}
    </div>
    <div class="clearfix"></div>
    <!-- Default box -->
    <div class="card">
        <div class="card-header">            
            <h3 class="card-title"><i class="fa fa-user"></i> Detail {{ $var['title'] }}</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">            
            <div class="form-group row">
                <div class="col-12 mb-2">
                    <center>
                        <img height="128" width="128" class="img img-circle" src="{{ $data->profile->path != null ? url("storage/".$data->profile->path) : asset('image/default.png') }}" alt="Foto Profile">
                    </center>
                    <center>
                        <label for="">Foto Profil</label>
                    </center>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-borderless">
                    <tr>                    
                        <th>Nama</th>
                        <td>:</td>
                        <td>{{ $data->nama }}</td>
                    </tr>
                    <tr>
                        <th>Jenis Kelamin</th>
                        <td>:</td>
                        <td>{{ $data->jenis_kelamin == 'L' ? 'Laki - Laki' : 'Perempuan' }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>:</td>
                        <td>{{ $data->email }}</td>
                    </tr>
                    <tr>
                        <th>No Telepon</th>
                        <td>:</td>
                        <td>{{ $data->no_telepon }}</td>
                    </tr>
                    <tr>
                        <th>Tempat Lahir</th>
                        <td>:</td>
                        <td>{{ $data->tempat_lahir }}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{ Carbon\Carbon::parse($data->tanggal_lahir)->format('m/d/Y') }}</td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>:</td>
                        <td>{{ $data->alamat ?? '-' }}</td>
                    </tr>
                    <tr>
                        <th>Latitude</th>
                        <td>:</td>
                        <td>{{ $data->latitude ?? '-' }}</td>
                    </tr>
                    <tr>
                        <th>Longitude</th>
                        <td>:</td>
                        <td>{{ $data->longitude ?? '-' }}</td>
                    </tr>
                    @if(count($data->foto_tambahan) > 0)
                    <tr>
                        <th>Foto Tambahan</th>
                        <td>:</td>
                            <td>
                            @foreach ($data->foto_tambahan as $item)
                                <img src="{{ url('storage/'.$item->path) }}" alt="" height="128" width="128">
                            @endforeach
                        </td>
                    </tr>
                    @endif
                    @if(count($data->file) > 0)
                    <tr>
                        <th>File Tambahan</th>
                        <td>:</td>
                        <td>
                            <ol>
                                @foreach ($data->file as $item)
                                    <li>
                                        <a target="_blank" href="{{ url('storage/'.$item->path) }}">{{ $item->name }}</a>
                                    </li>
                                @endforeach
                            </ol>
                        </td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection