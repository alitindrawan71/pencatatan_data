<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $table = 'image';

    protected $fillable = [
        'name',
        'path',
        'is_profile',
    ];

    public function data(){
        return $this->belongsTo('App\Data','data_id');
    }
}
