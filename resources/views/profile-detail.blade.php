@extends('layouts.app',['active' => 'profil'])
<style>
    .pagination {
        justify-content: center;
    }
    #map{
      height: 500px;
    }
    .pac-card {
      margin: 10px 10px 0 0;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      background-color: #fff;
      font-family: Roboto;
    }
  
    #pac-container {
      padding-bottom: 12px;
      margin-right: 12px;
    }
  
    .pac-controls {
      display: inline-block;
      padding: 5px 11px;
    }
  
    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }
  
    #pac-input:focus {
      border-color: #4d90fe;
    }
  
    #pac-input, #btn-checkpos {
      margin-bottom: 10px;
    }
    .lb-data .lb-caption a{        
        color: #ffffff !important;
    }
    .table tbody>tr>th{
        width:200px !important;
    }
    .table tbody>tr>td{
        width:auto !important;
    }
</style>
@section('content')
<main>
    <section id="call-to-action" data-aos="fade-in" data-aos-delay="50">
        <div class="container text-center">
          <div class="section-header" style="padding-top: 100px;">
            <h3>Detail Profil</h3>
          </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="card mt-3">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-12 mb-2">
                            <center>
                                <a class="example-image-link" data-title="<div class='text-center' style='text-decoration:none;'><a target='_blank' download='{{ $data->profile->name }}' class='btn btn-sm btn-success' href='{{ $data->profile->path != null ? url("storage/".$data->profile->path) : asset('image/default.png') }}'><i class='fa fa-download'></i> Download</a></div>" href="{{ $data->profile->path != null ? url("storage/".$data->profile->path) : asset('image/default.png') }}" data-lightbox="example-1" >
                                    <img height="128" width="128" class="img" src="{{ $data->profile->path != null ? url("storage/".$data->profile->path) : asset('image/default.png') }}" alt="Foto Profile">
                                </a>
                            </center>
                            <div class="table-responsive">
                                <table class="table table-hover table-borderless">
                                    <tbody>
                                        <tr>                    
                                            <th>Nama</th>
                                            <td>:</td>
                                            <td>{{ $data->nama }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jenis Kelamin</th>
                                            <td>:</td>
                                            <td>{{ $data->jenis_kelamin == 'L' ? 'Laki - Laki' : 'Perempuan' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td>{{ $data->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>No Telepon</th>
                                            <td>:</td>
                                            <td>{{ $data->no_telepon }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tempat Lahir</th>
                                            <td>:</td>
                                            <td>{{ $data->tempat_lahir }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Lahir</th>
                                            <td>:</td>
                                            <td>{{ Carbon\Carbon::parse($data->tanggal_lahir)->format('m/d/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>:</td>
                                            <td>
                                                <a href="http://maps.google.com/maps?q={{$data->latitude}},{{ $data->longitude }}" target="_blank">
                                                    {{ $data->alamat ?? '-' }}
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Lokasi</th>
                                            <td>:</td>
                                            <td><div id="map"></div></td>
                                        </tr>                                    
                                        @if(count($data->foto_tambahan) > 0)
                                        <tr>
                                            <th>Foto Tambahan</th>
                                            <td>:</td>
                                                <td>
                                                @foreach ($data->foto_tambahan as $item)
                                                    <a class="example-image-link" href="{{ url('storage/'.$item->path) }}" data-lightbox="example-set" data-title="<div class='text-center' style='text-decoration:none;'><a target='_blank' download='{{ $item->name }}' class='btn btn-sm btn-success' href='{{ url('storage/'.$item->path) }}'><i class='fa fa-download'></i> Download</a></div>">
                                                        <img class="example-image" src="{{ url('storage/'.$item->path) }}" alt="" height="128" width="128">
                                                    </a>
                                                @endforeach
                                            </td>
                                        </tr>
                                        @endif
                                        @if(count($data->file) > 0)
                                        <tr>
                                            <th>File Tambahan</th>
                                            <td>:</td>
                                            <td>
                                                <ol>
                                                    @foreach ($data->file as $item)
                                                        <li>
                                                            <a target="_blank" href="{{ url('storage/'.$item->path) }}">{{ $item->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@section('javascripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,geometry,drawing&callback=initMap"  async defer></script>
<script>
    var data = {!! json_encode($data->toArray()) !!};
    function initMap() {
        var lat_ = parseFloat(data.latitude);
        var lng_ = parseFloat(data.longitude);
        var myLatLng = {lat: lat_, lng: lng_}        
    
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatLng,
        });
    
        var infowindow = new google.maps.InfoWindow();
        var marker, i;        
        
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(data.latitude), parseFloat(data.longitude)),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                info = "<div>Nama : "+data.nama+"</div><div>Alamat : "+data.alamat+"</div";
                infowindow.setContent(info);
                infowindow.open(map, marker);
            }
        })(marker, i));        
    }
    
</script>
@endsection