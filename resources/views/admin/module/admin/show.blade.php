@extends('admin.layouts.app', ['active' => 'Admin'])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Admin</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Admin</a></li>
            <li class="breadcrumb-item active">Show</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="float-right mb-3">
        <a href="{{ route('admin.edit', $data->id) }}"><button class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit Admin</button></a>        
    </div>
    <div class="clearfix"></div>
    <!-- Default box -->
    <div class="card">
        <div class="card-header">            
            <h3 class="card-title"><i class="fa fa-user"></i> Detail Admin</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">            
            <div class="table-responsive">
                <table class="table table-hover table-borderless">
                    <tr>                    
                        <th>Username</th>
                        <td>:</td>
                        <td>{{ $data->username }}</td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td>:</td>
                        <td>********</td>
                    </tr>                    
                </table>
            </div>
        </div>
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection