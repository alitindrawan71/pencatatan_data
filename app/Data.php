<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Data extends Model
{
    //
    protected $table = 'data';

    protected $fillable = [
        'nama',
        'jenis_kelamin',
        'email',
        'no_telepon',
        'tempat_lahir',
        'tanggal_lahir',
        'alamat',
        'latitude',
        'longitude',
    ];

    public function image(){
        return $this->hasMany('App\Image','data_id');
    }

    public function file(){
        return $this->hasMany('App\File','data_id');
    }

    public function profile(){
        return $this->hasOne('App\Image','data_id')->where('is_profile',1);
    }

    public function foto_tambahan(){
        return $this->hasMany('App\Image','data_id')->where('is_profile',0);
    }
}
