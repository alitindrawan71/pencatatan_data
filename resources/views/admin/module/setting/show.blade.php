@extends('admin.layouts.app', ['active' => 'Pengaturan'])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Pengaturan</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('setting.index') }}">Pengaturan</a></li>
            <li class="breadcrumb-item active">Show</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="float-right mb-3">
        <a href="{{ route('setting.edit', $data->id) }}"><button class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit Pengaturan</button></a>        
    </div>
    <div class="clearfix"></div>
    <!-- Default box -->
    <div class="card">
        <div class="card-header">            
            <h3 class="card-title"><i class="nav-icon fas fa-cog"></i> Detail Pengaturan</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">            
            <div class="table-responsive">
                <table class="table table-hover table-borderless">
                    <tr>                    
                        <th>Key</th>
                        <td>:</td>
                        <td>{{ $data->key }}</td>
                    </tr>
                    <tr>                    
                        <th>Deskripsi</th>
                        <td>:</td>
                        <td>{{ $data->desc }}</td>
                    </tr>
                    <tr>                    
                        <th>Value</th>
                        <td>:</td>
                        <td>{{ $data->value }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection