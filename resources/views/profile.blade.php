@extends('layouts.app',['active' => 'profil'])
<style>
    .pagination {
        justify-content: center;
    }
    #map{
      height: 500px;
    }
    .pac-card {
      margin: 10px 10px 0 0;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      background-color: #fff;
      font-family: Roboto;
    }
  
    #pac-container {
      padding-bottom: 12px;
      margin-right: 12px;
    }
  
    .pac-controls {
      display: inline-block;
      padding: 5px 11px;
    }
  
    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }
  
    #pac-input:focus {
      border-color: #4d90fe;
    }
  
    #pac-input, #btn-checkpos {
      margin-bottom: 10px;
    }    
</style>
@section('content')
<main>
    <section id="call-to-action" data-aos="fade-in" data-aos-delay="50">
        <div class="container text-center">
          <div class="section-header" style="padding-top: 100px;">
            <h3>Profil</h3>
          </div>
        </div>
    </section>
    
    <section id="portfolio" class="section-bg" style="overflow: inherit">
        <div class="container" data-aos="fade-up">

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class=" col-lg-12">
                    <ul id="portfolio-flters">
                        <li data-filter="filter-list" class="filter-active">List</li>
                        <li data-filter="filter-map">Map</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                <div class="col-12 li-filter" id="filter-list">
                    <div class="container">
                        <div class="card text-center mb-5">
                            <div class="card-body col-lg-10 mx-auto">
                                <h5 class="card-title font-weight-bold">Profil List</h5>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <select name="tampilkan" id="tampilkan" class="form-control" onchange="change_page(this.value)">
                                                <option value="10" {{ $data->toArray()['per_page'] == 10 ? 'selected' : '' }}>10</option>
                                                <option value="25" {{ $data->toArray()['per_page'] == 25 ? 'selected' : '' }}>25</option>
                                                <option value="50" {{ $data->toArray()['per_page'] == 50 ? 'selected' : '' }}>50</option>
                                                <option value="100" {{ $data->toArray()['per_page'] == 100 ? 'selected' : '' }}>100</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="input-group">                                                
                                                <input type="text" placeholder="Cari nama" name="search" id="search" class="form-control" value="{{ $_GET['search'] ?? '' }}" onkeyup="update_link(this.value)" onkeypress="runScript(event)">
                                                <div class="input-group-append">
                                                    <a id="search_btn" href="{{ url()->current() }}?search=" class="btn btn-sm btn-primary">Cari</a>
                                                </div>                                                                                     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @forelse ($data as $item)
                                <a href="{{ url('profil/detail/'.$item->id) }}" style="text-decoration: none !important; color:black !important">
                                    <div class="card row mb-3 card-profile" style="flex-direction: row !important">
                                        <div class="card-header border-0 col-xs-12 col-md-3">
                                            <img src="{{  $item->profile->path != null ? url("storage/".$item->profile->path) : asset('image/default.png') }}" height="128" width="128" alt="profil" class="img img-circle" style="object-fit: cover;">
                                        </div>
                                        <div class="text-left col-md-9 col-xs-12">
                                            <p class="plain-text">Nama      : {{ $item->nama }}</p>
                                            <p class="plain-text">Email     : {{ $item->email }}</p>
                                            <p class="plain-text">Telepon   : {{ $item->no_telepon }}</p>
                                        </div>
                                        <div class="w-100"></div>
                                    </div>
                                </a>
                                @empty
                                    <p>Tidak ada data ditemukan</p>
                                @endforelse                             
                            </div>                            
                            <div class="mx-auto">
                                {{ $data->render() }}
                            </div>
                        </div>                        
                    </div>                    
                </div>

                <div class="col-12 li-filter" id="filter-map" style="display: none">
                    <div class="container">
                        <div class="card text-center mb-5">
                            <div class="card-body col-lg-10 mx-auto">
                                <h5 class="card-title font-weight-bold">Map</h5>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Section -->            
</main>
@endsection
@section('javascripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,geometry,drawing&callback=initMap"  async defer></script>
<script>
    var data = {!! json_encode($data->toArray()['data']) !!};
    function initMap() {
        var lat_ = -8.6030957;
        var lng_ = 115.1786036;
        var myLatLng = {lat: lat_, lng: lng_}
    
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatLng,
        });
    
        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        data.forEach(function(item) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    info = "<div>Nama : "+item.nama+"</div><div>Alamat : "+item.alamat+"</div";
                    infowindow.setContent(info);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        });
    }

    function change_page(val){
        let url = "{{ url('profil') }}/"+val;
        if($('#search').val() != '')
            url += '?search='+$('#search').val();
        window.location = url;
    }

    function update_link(val){
        $('#search_btn').attr('href',"{{ url()->current() }}?search="+val);
    }

    function runScript(e) {        
        if (e.keyCode == 13) {            
            $('#search_btn')[0].click();
        }
    }
</script>
@endsection