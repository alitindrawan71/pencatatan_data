<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Data;

class ProfileController extends Controller
{
    //
    public function index($page = null){
        if(isset($_GET['search']))
            $data = Data::with(['profile'])->whereRaw('lower(nama) like ?','%'.$_GET['search'].'%')->latest()->paginate($page ?? 10);
        else
            $data = Data::with(['profile'])->latest()->paginate($page ?? 10);
        // $map = $data->toArray();
        // dd($map);
        return view('profile',compact('data'));
    }

    public function detail($id){
        $data = Data::with(['profile'])->find($id);
        return view('profile-detail', compact('data'));
    }
}
