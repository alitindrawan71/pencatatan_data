@extends('admin.layouts.app', ['active' => 'Dashboard'])
<style>
    #map{
      height: 500px;
    }
    .pac-card {
      margin: 10px 10px 0 0;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      background-color: #fff;
      font-family: Roboto;
    }
  
    #pac-container {
      padding-bottom: 12px;
      margin-right: 12px;
    }
  
    .pac-controls {
      display: inline-block;
      padding: 5px 11px;
    }
  
    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }
  
    #pac-input:focus {
      border-color: #4d90fe;
    }
  
    #pac-input, #btn-checkpos {
      margin-bottom: 10px;
    }  
  </style>
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Dashboard</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">        
        <div class="row">
            <div class="col-lg-3 col-6">            
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ App\Data::count() }}</h3>
                        <p>Data User</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-user"></i>
                    </div>
                    <a href="{{ route('user.index') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ App\User::count() }}</h3>
                        <p>Admin</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-user-shield"></i>
                    </div>
                    <a href="{{ route('admin.index') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-12 col-12">
                <div id="map"></div>
            </div>
        </div> 
    </div>
</section>
<!-- /.content -->
@endsection
@section('javascripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,geometry,drawing&callback=initMap"  async defer></script>
<script>
    var data = {!! json_encode($data) !!};
    function initMap() {
        var lat_ = -8.6030957;
        var lng_ = 115.1786036;
        var myLatLng = {lat: lat_, lng: lng_}
    
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatLng,
        });
    
        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        data.forEach(function(item) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    info = "<div>Nama : "+item.nama+"</div><div>Alamat : "+item.alamat+"</div";
                    infowindow.setContent(info);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        });
    }
</script>
@endsection