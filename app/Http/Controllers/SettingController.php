<?php

namespace App\Http\Controllers;

use Validator;
use DB;

use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Setting;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class SettingController extends Controller
{
    //
    public function index(){
        return view('admin.module.setting.index');
    }

    public function create(){
        return view('admin.module.setting.create');
    }

    public function store(Request $request){
        $messages = [
            'key.required' => 'Key tidak boleh kosong',
            // 'value.required' => 'Value tidak boleh kosong',
            'type.required' => 'Tipe tidak boleh kosong',
            'key.unique' => 'Key sudah terdaftar di database',            
        ];  
        $validator = Validator::make($request->input(), array(
            "key"           => 'required|unique:setting',
            // "value"         => 'required',
            "type"         => 'required',
        ), $messages);
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }

        try {
            DB::beginTransaction();

            if($request->type == 'file'){
                $destination = 'public/setting/';
                if ($request->hasFile('file'))
                {
                    if (!file_exists(storage_path($destination))) {
                        Storage::makeDirectory($destination);
                    }                    
                                        
                    $file = $request->file('file');
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$file->getClientOriginalExtension();
                    $file->storeAs($destination, $file_name);
                    $value = 'setting/'.$file_name;
                }
            }
            else{
                $value = $request->value;
            }            

            Setting::create([
                'key' => $request->key,
                'desc' => $request->desc,
                'type' => $request->type,
                'value' => $value,
                'status' => 1,
            ]);

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function show($id){
        $data = Setting::find($id);
        return view('admin.module.setting.show',compact('data'));
    }

    public function edit($id){
        $data = Setting::find($id);
        return view('admin.module.setting.edit',compact('data'));
    }

    public function update($id,Request $request){
        $messages = [
            // 'key.required' => 'Key tidak boleh kosong',
            // 'value.required' => 'Value tidak boleh kosong',
            // 'key.unique' => 'Key sudah terdaftar di database',            
        ];  
        $validator = Validator::make($request->input(), array(
            // "key"           => 'required|unique:setting,key,'.$id,
            // "value"         => 'required',
        ), $messages);

        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }

        try {
            DB::beginTransaction();

            if(Setting::find($id)->type == 'file'){
                $destination = 'public/setting/';
                if ($request->hasFile('file'))
                {
                    if (!file_exists(storage_path($destination))) {
                        Storage::makeDirectory($destination);
                    }                    
                                        
                    $file = $request->file('file');
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$file->getClientOriginalExtension();
                    $file->storeAs($destination, $file_name);
                    $value = 'setting/'.$file_name;
                }else{
                    $value = Setting::find($id)->value;
                }
            }
            else{
                $value = $request->value;
            }            


            Setting::find($id)->update([
                // 'key' => $request->key,
                'desc' => $request->desc,
                'value' => $value,
            ]);            

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function delete($id){
        try {
            Setting::find($id)->delete();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }   
    }

    public function getData(){
        $query = Setting::select('*')->latest();
        return Datatables::of($query)
        ->addIndexColumn()        
        ->editColumn('status', function($data){
            $value = $data->status == 1 ? 'checked' : '';
            return "<input type='checkbox' $value data-bootstrap-switch data-size='xs' data-id='".$data->id."' >";
        })
        ->addColumn('action', function($data){
            return "
                <a href=".url('setting/'.$data->id)."><button class='btn btn-sm btn-secondary'><i class='fa fa-eye'></i><span class='d-none d-sm-inline'> Lihat</span></button></a>
                <a href=".url('setting/'.$data->id.'/edit')."><button class='btn btn-sm btn-success'><i class='fa fa-edit'></i><span class='d-none d-sm-inline'> Edit</span></button></a>
                <button onclick='del(".$data->id.",`".$data->username."`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i><span class='d-none d-sm-inline'> Hapus</span></button>
            ";
        })
        ->rawColumns([
            'action','status'
        ])
        ->make(true);   
    }

    public function toggleActive(Request $request){
        try {            
            $status = $request->status == "true" ? 1 : 0 ;
            Setting::find($request->id)->update(['status' => $status]);
            return response()->json(['status'=>true]);
        } catch (\Throwable $th) {
            return response()->json(['status'=>false], 400);
        }
    }
}
