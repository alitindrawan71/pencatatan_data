<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $table = 'file';

    protected $fillable = [
        'name',
        'path',
        'size',
        'mime_type',
    ];
    
    public function data(){        
        return $this->belongsTo('App\Data','data_id');
    }
}