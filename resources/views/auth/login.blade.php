@extends('layouts.app',['active' => 'login'])

@section('content')
<main id="main">
  
    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" data-aos="fade-in" data-aos-delay="50">
      <div class="container text-center">
       <!--  <h3>Call To Action</h3>
        <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <a class="cta-btn" href="#">Call To Action</a> -->

        <div class="section-header" style="padding-top: 100px;">
          <h3>Login</h3>
        </div>
      </div>
    </section><!-- #call-to-action -->
    
    <section id="contact" class="section-bg wow fadeInUp" data-aos="fade-up" data-aos-delay="50">
      <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="form">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif
                            <div class="col-xs-1" align="center">
                            <div class="form-group col-md-10 pt-5">
                                <input style="margin-bottom: 30px;" type="text" name="username" class="form-control" id="username" placeholder="Username"/>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                                <br>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </section><!-- #contact -->

  </main>
@endsection
