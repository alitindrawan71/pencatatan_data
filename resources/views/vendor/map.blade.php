<style>
    #map{
      height: 500px;
    }
    .pac-card {
      margin: 10px 10px 0 0;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      background-color: #fff;
      font-family: Roboto;
    }
  
    #pac-container {
      padding-bottom: 12px;
      margin-right: 12px;
    }
  
    .pac-controls {
      display: inline-block;
      padding: 5px 11px;
    }
  
    .pac-controls label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }
  
    #pac-input:focus {
      border-color: #4d90fe;
    }
  
    #pac-input, #btn-checkpos {
      margin-bottom: 10px;
    }  
  </style>
  <a id="btn-checkpos" href="javascript:void(0)" class="btn btn-info btn-sm"><i class="fa fa-map-pin"></i> Posisi Saat Ini</a>
  <p class="font-weight-bold">Drag dan Drop <font color="red">PIN MERAH</font> untuk mencari lokasi sesuai dengan map.</p>
  {{-- <p class="font-weight-bold">Drag dan Drop <font color="red">PIN MERAH</font> untuk mencari lokasi sesuai dengan map, atau ketik pencarian di kolom Cari Lokasi dibawah ini.</p> --}}
  {{-- <input type="text" class="form-control" id="pac-input" placeholder="Cari Lokasi"> --}}
  <div id="map"></div>