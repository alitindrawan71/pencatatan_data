<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;
Use Illuminate\Support\Facades\DB;
Use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

use App\Data;
use App\Image;
use App\File;

use Carbon\Carbon;

class DataController extends Controller
{
    public $var;
    public function __construct(){
        $this->var['title'] = "User";
    }

    public function index(){
        $var = $this->var;
        return view('admin.module.user.index', compact('var'));
    }

    public function create(){
        $var = $this->var;
        return view('admin.module.user.create', compact('var'));
    }

    public function store(Request $request){
        $messages = [
            'nama.required' => 'Nama tidak boleh kosong',
            'jenis_kelamin.required' => 'Jenis kelamin belum dipilih',
            'email.required' => 'Email tidak boleh kosong, isi dengan "-" jika tidak ada email',
            'no_telepon.required' => 'No telepon tidak boleh kosong',
            'tempat_lahir.required' => 'Tempat lahir tidak boleh kosong',
            'tanggal_lahir.required' => 'Tanggal lahir tidak boleh kosong',
        ];  
        $validator = Validator::make($request->input(), array(
            "nama"              => 'required',
            "jenis_kelamin"     => 'required',
            "email"             => 'required',
            "no_telepon"        => 'required',
            "tempat_lahir"      => 'required',
            "tanggal_lahir"     => 'required',
            // "foto_profil"       => 'required',
        ), $messages);
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }
            
        try {
            DB::beginTransaction();
            $data_user = Data::create([
                'nama'          => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email'         => $request->email,
                'no_telepon'    => str_replace('_','',str_replace('-','',$request->no_telepon)),
                'tempat_lahir'  => $request->tempat_lahir,
                'tanggal_lahir' => Carbon::parse($request->tanggal_lahir)->format('Y-m-d'),
                'alamat'        => $request->alamat,
                'latitude'      => $request->latitude,
                'longitude'     => $request->longitude,
            ]);

            $status = $file_name = null;
                
            // Foto Profil
            $destination = 'public/user/profil/';
            if ($request->hasFile('foto_profil'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                $file = $request->file('foto_profil');
                $extention = $file->getClientOriginalExtension();
                $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                $file->storeAs($destination, $file_name);
                $file_path = 'user/profil/'.$file_name;
                $file_name = $file->getClientOriginalName();
            }else{
                $file_name = 'default.png';
                $file_path = null;
            }

            $data_user->image()->create([
                'name'      => $file_name,
                'path'      => $file_path,
                'is_profile' => 1,                
            ]);

            // Foto Tambahan
            $destination = 'public/user/tambahan/';
            if ($request->hasFile('foto_tambahan'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                foreach($request->file('foto_tambahan') as $temp){
                    $file = $temp;
                    $extention = $file->getClientOriginalExtension();
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                    $file->storeAs($destination, $file_name);
                    $data['name'] = $file->getClientOriginalName();
                    $data['path'] = 'user/tambahan/'.$file_name;
                    $data_user->image()->create($data);
                }
            }

            // File
            $destination = 'public/user/file/';
            if ($request->hasFile('file'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                foreach($request->file('file') as $temp){
                    $file = $temp;
                    $extention = $file->getClientOriginalExtension();
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                    $file->storeAs($destination, $file_name);
                    $data['name']       = $file->getClientOriginalName();
                    $data['size']       = $file->getSize();
                    $data['path']       = 'user/file/'.$file_name;
                    $data['mime_type']  = $extention;

                    $data_user->file()->create($data);
                }
            }

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function show($id){
        $var = $this->var;
        $data = Data::with(['foto_tambahan','file','profile'])->find($id);
        // dd($data->profile->path);
        return view('admin.module.user.show', compact('var','data'));
    }

    public function edit($id){
        $var = $this->var;
        $data = Data::with(['foto_tambahan','file','profile'])->find($id);
        return view('admin.module.user.edit', compact('var','data'));
    }
    

    public function update($id,Request $request){
        $messages = [
            'nama.required' => 'Nama tidak boleh kosong',
            'jenis_kelamin.required' => 'Jenis kelamin belum dipilih',
            'email.required' => 'Email tidak boleh kosong, isi dengan "-" jika tidak ada email',
            'no_telepon.required' => 'No telepon tidak boleh kosong',
            'tempat_lahir.required' => 'Tempat lahir tidak boleh kosong',
            'tanggal_lahir.required' => 'Tanggal lahir tidak boleh kosong',
        ];  
        $validator = Validator::make($request->input(), array(
            "nama"              => 'required',
            "jenis_kelamin"     => 'required',
            "email"             => 'required',
            "no_telepon"        => 'required',
            "tempat_lahir"      => 'required',
            "tanggal_lahir"     => 'required',
            // "foto_profil"       => 'required',
        ), $messages);
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }
            
        try {
            DB::beginTransaction();
            $data_user = Data::find($id);
            $data_user->update([
                'nama'          => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email'         => $request->email,
                'no_telepon'    => str_replace('_','',str_replace('-','',$request->no_telepon)),
                'tempat_lahir'  => $request->tempat_lahir,
                'tanggal_lahir' => Carbon::parse($request->tanggal_lahir)->format('Y-m-d'),
                'alamat'        => $request->alamat,
                'latitude'      => $request->latitude,
                'longitude'     => $request->longitude,
            ]);

            $status = $file_name = null;
                
            // Foto Profil
            $destination = 'public/user/profil/';
            if ($request->hasFile('foto_profil'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                $file = $request->file('foto_profil');
                $extention = $file->getClientOriginalExtension();
                $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                $file->storeAs($destination, $file_name);
                $file_path = 'user/profil/'.$file_name;
                $file_name = $file->getClientOriginalName();

                $image = Image::find($data_user->profile->id);

                $temp = $image->path;                

                $image->update([
                    'name'      => $file_name,
                    'path'      => $file_path,
                ]);

                if(file_exists('storage/'.$temp))
                unlink('storage/'.$temp);
            }

            

            // Foto Tambahan
            $destination = 'public/user/tambahan/';
            if ($request->hasFile('foto_tambahan'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                foreach($request->file('foto_tambahan') as $temp){
                    $file = $temp;
                    $extention = $file->getClientOriginalExtension();
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                    $file->storeAs($destination, $file_name);
                    $data['name'] = $file->getClientOriginalName();
                    $data['path'] = 'user/tambahan/'.$file_name;
                    $data_user->image()->create($data);
                }
            }

            // File
            $destination = 'public/user/file/';
            if ($request->hasFile('file'))
            {
                if (!file_exists(storage_path($destination))) {
                    Storage::makeDirectory($destination);
                }

                foreach($request->file('file') as $temp){
                    $file = $temp;
                    $extention = $file->getClientOriginalExtension();
                    $file_name = md5($file->getClientOriginalName())."_unique_".uniqid().".".$extention;
                    $file->storeAs($destination, $file_name);
                    $data['name']       = $file->getClientOriginalName();
                    $data['size']       = $file->getSize();
                    $data['path']       = 'user/file/'.$file_name;
                    $data['mime_type']  = $extention;

                    $data_user->file()->create($data);
                }
            }

            DB::commit();
            return response()->json(['status' => true,'image'=>$data_user->foto_tambahan,'file'=>$data_user->file]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function delete($id){
        try {
            DB::beginTransaction();

            $data = Data::find($id);
            $temp = [];
            foreach($data->image as $item){
                array_push($temp, $item->path);
            }
            foreach($data->file as $item){
                array_push($temp, $item->path);
            }
            $data->delete();

            foreach ($temp as $key => $item) {
                if(file_exists('storage/'.$item))
                    unlink('storage/'.$item);
                
            }

            DB::commit();
            return response()->json(['status'=>true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$th->getMessage() ],400);
        }        
    }

    public function deleteImage($id){
        try {
            DB::beginTransaction();

            $data = Image::find($id);
            $temp = $data->path;
            $data->delete();

            if(file_exists('storage/'.$temp))
                unlink('storage/'.$temp);

            DB::commit();
            return response()->json(['status'=>true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$th->getMessage() ],400);
        }        
    }

    public function deleteFile($id){
        try {
            DB::beginTransaction();

            $data = File::find($id);
            $temp = $data->path;
            $data->delete();

            if(file_exists('storage/'.$temp))
                unlink('storage/'.$temp);

            DB::commit();
            return response()->json(['status'=>true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$th->getMessage() ],400);
        }        
    }

    public function getData(){
        $query = Data::with(['profile'])->select('*')->latest();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('profil', function($data){
            if($data->profile != null)            
                return "<img height='64' width='64' class='img img-circle' src='".($data->profile->path != null ? url("storage/".$data->profile->path) : asset('image/default.png'))."' alt='Foto Profil'>";
            else
                return "<img height='64' width='64' class='img img-circle' src='".asset('image/default.png')."' alt='Foto Profil'>";
        })
        ->addColumn('action', function($data){
            return "
                <a href=".url('user/'.$data->id)."><button class='btn btn-sm btn-secondary'><i class='fa fa-eye'></i><span class='d-none d-sm-inline'> Lihat</span></button></a>
                <a href=".url('user/'.$data->id.'/edit')."><button class='btn btn-sm btn-success'><i class='fa fa-edit'></i><span class='d-none d-sm-inline'> Edit</span></button></a>
                <button onclick='del(".$data->id.",`".$data->nama."`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i><span class='d-none d-sm-inline'> Hapus</span></button>
            ";
        })
        ->rawColumns([
            'action','profil',
        ])
        ->make(true);
    }
}
