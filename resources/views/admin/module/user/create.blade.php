@extends('admin.layouts.app', ['active' => $var['title']])

@section('content')

<style>
.input-file {
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $var['title'] }}</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ $var['title'] }}</a></li>
            <li class="breadcrumb-item active">Create</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-user"></i> Tambah {{ $var['title'] }}</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('user.store') }}" method="post" class="form-horizontal" id="formData" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <div class="col-12 mb-2">
                        <center>
                            <label title="Ganti Foto Profil" for="foto_profil" style="cursor:pointer;">
                                <img id="imgPreview2" src="{{ asset('image/default.png') }}" alt="Foto Profil" class="img img-circle" height="128" width="128">                            
                            </label>
                        </center>
                        <center>                            
                            <input onchange="showPreview2(this)" type="file" name="foto_profil" id="foto_profil" class="input-file" />
                            <label title="Ganti Foto Profil" for="foto_profil" style="cursor:pointer;">Foto Profil <i class="fa fa-edit"></i></label>
                        </center>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <label class="radio-inline mr-2"> <input type="radio" name="jenis_kelamin" value="L"> Laki - Laki </label>
                        <label class="radio-inline"> <input type="radio" name="jenis_kelamin" value="P"> Perempuan </label>
                    </div>
                </div>                

                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="email" name="email" placeholder="Contoh : user@gmail.com">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="no_telepon" class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="no_telepon" name="no_telepon" placeholder="Contoh : 0891-1122-2334" data-inputmask='"mask": "9999-9999-9999-9999"' data-mask>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat lahir">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal lahir dengan format dd-mm-yyyy" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <input type="text" name="alamat" id="pac-input" class="form-control" placeholder="Alamat Lengkap">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="location" class="col-sm-2 col-form-label">Lokasi</label>
                    <div class="col-sm-10">
                        @include('vendor.map')
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lat" class="col-sm-2 col-form-label">Latitude</label>
                    <div class="col-sm-10">
                        <input type="text" name="latitude" id="lat" class="form-control" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lng" class="col-sm-2 col-form-label">Longitude</label>
                    <div class="col-sm-10">
                        <input type="text" name="longitude" id="lng" class="form-control" readonly>
                    </div>
                </div>

                <div id="form_foto_optional">
                    <div class="form-group row">
                        <label for="foto_tambahan" class="col-sm-2 col-form-label">Foto Tambahan <small class=""> *Optional (Maks:3)</small></label>
                        <div class="col-sm-10">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input foto foto_tambahan" id="foto_tambahan[]" name="foto_tambahan[]" multiple>
                                <label id="foto_tambahan_label" class="custom-file-label" for="foto_tambahan">Pilih Gambar</label>
                            </div>
                        <small class="form-text text-muted"><i class="fas fa-exclamation-circle"></i>&nbsp;File bertipe JPG/PNG dengan ukuran maksimal 1 MB</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="file" class="col-sm-2 col-form-label">File<small class=""> *Optional (Maks:3)</small></label>
                    <div class="col-sm-10">
                        <div class="custom-file">                            
                            <input type="file" class="custom-file-input file" id="file" name="file[]" multiple>
                            <label id="file_label" class="custom-file-label" for="file">Pilih File</label>
                        </div>                      
                      <small class="form-text text-muted"><i class="fas fa-exclamation-circle"></i>&nbsp;File bertipe PDF dengan ukuran maksimal 3 MB</small>
                    </div>                    
                </div>
                

                <div class="float-right">
                    <button id="submit" class="btn btn-success" type="submit">Simpan</button>
                </div>

            </form> 
        </div>    
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <img id="imgPreview" src="{{ asset('image/default.png') }}" alt="your image" class="img-fluid"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>

@endsection

@section('javascripts')
<script>
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000
        });

    function showPreview(input){
        readURL(document.getElementById(input));
        $('#myModal').modal('show');
    }

    function showPreview2(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {                
                $('#imgPreview2').attr('src', e.target.result);
            }   
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {                
                $('#imgPreview').attr('src', e.target.result);
            }   
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }else{
            $('#imgPreview').attr('src', "{{ asset('image/default.png') }}");
        }
    }

    function tambahButton(){
        count = $('.foto_tambahan').length;
        if(count >= 3){
            Toast.fire({
                icon: 'info',
                title: 'Maksimal jumlah foto tambahan adalah 3'
            });
        }else{
            temp = document.getElementsByTagName("template")[0];
            clon = temp.content.cloneNode(true);                
            $('#form_foto_optional').append(clon);
        }
    }

    function hapusButton(input){
        $(input).closest('div.row').remove();
    }
    

    $(document).ready(function(){

        $('input.foto').change(function(e){
            var files = e.originalEvent.target.files;
            if(files.length > 3){
                Toast.fire({
                    icon: 'error',
                    title: 'Maksimal jumlah gambar adalah 3'
                });
                $(this).siblings('label').html("Pilih Gambar");
                $(this).val('');
            }
            for (var i=0, len=files.length; i<len; i++){
                    var n = files[i].name;
                    var s = files[i].size;
                    var t = files[i].type;                
                if (s > 1024000) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Maksimal ukuran gambar adalah 1 MB'
                    });
                    $(this).siblings('label').html("Pilih Gambar");
                    $(this).val('');
                }else if(t != 'image/png' && t != 'image/jpeg'){
                    Toast.fire({
                        icon: 'error',
                        title: 'Format gambar harus JPG/PNG'
                    });
                    $(this).siblings('label').html("Piilh Gambar");
                    $(this).val('');
                }
            }
            if(files.length > 1)
                $(this).siblings('label').html(files.length+" Gambar dipilih");
            else
                $(this).siblings('label').html(n);
        });

        $('input.file').change(function(e){                        
            var files = e.originalEvent.target.files;
            if(files.length > 3){
                Toast.fire({
                    icon: 'error',
                    title: 'Maksimal jumlah file adalah 3'
                });
                $(this).siblings('label').html("Pilih File");
                $(this).val('');
            }
            for (var i=0, len=files.length; i<len; i++){
                    var n = files[i].name;
                    var s = files[i].size;
                    var t = files[i].type;
                if (s > 3072000) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Maksimal ukuran setiap file adalah 3 MB'
                    });
                    $(this).siblings('label').html("Pilih File");
                    $(this).val('');
                }else if(t != 'application/pdf'){
                    Toast.fire({
                        icon: 'error',
                        title: 'Format file harus pdf'
                    });
                    $(this).siblings('label').html("Pilih File");
                    $(this).val('');
                }
            }
            if(files.length > 1)
                $(this).siblings('label').html(files.length+" File dipilih");
            else
                $(this).siblings('label').html(n);
        });

        $('#tanggal_lahir').datepicker({
            autoclose: true,
            format:'dd-mm-yyyy',
        });

        $('#tanggal_lahir').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        $('[data-mask]').inputmask()

        $('#formData').submit(function(e){
            e.preventDefault();            
            $('#submit').attr('disabled', true);
            $('#submit').html('Menyimpan <i class="fas fa-sync-alt fa-spin"></i>');   
            var formData = new FormData($(this)[0]);
            $.ajax({
                method : $(this).attr('method'),
                url : $(this).attr('action'),
                data : formData,
                processData: false,
                contentType: false,
                success : function(data){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');
                    Swal.fire(
                        'Selesai!',
                        'Data berhasil disimpan.',
                        'success'
                    )
                    $('#formData').trigger("reset");
                    $('#imgPreview2').attr('src',"{{ asset('image/default.png') }}");
                    $('#foto_profil_label').html('Pilih Gambar');
                    $('#foto_tambahan_label').html('Pilih Gambar');
                    $('#file_label').html('Pilih File');
                },
                error: function(err){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');                    
                    var response = JSON.parse(err.responseText);
                    
                    var errorString = '';
                    if(typeof response.message === 'object'){
                        $.each( response.message, function( key, value) {
                            errorString += value + "<br>";
                        });
                    }else{
                        errorString = response.message;
                    }
                    Swal.fire(
                        'Error!',
                        errorString,
                        'error'
                    )
                }
            })            

        });
    });    
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,geometry,drawing&callback=initMap"  async defer></script>
<script type="text/javascript">
    var map,marker,cur_pos;
    $(document).ready(function(){
      initMap();
  
      $('#pac-input').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      });
  
      $("#btn-checkpos").click(function(){
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            cur_pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
  
            $("#lat").val(position.coords.latitude);
            $("#lng").val(position.coords.longitude);
  
            map.setCenter(cur_pos);
            marker.setPosition(cur_pos);
          });
        }
      });
    });
  
    function initMap() {
      var lat_ = $("#lat").val()?parseFloat($("#lat").val()):-8.603117;
      var lng_ = $("#lng").val()?parseFloat($("#lng").val()):115.178797;
      var myLatLng = {lat: lat_, lng: lng_}
  
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: myLatLng,
      });
  
      marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        draggable:true,
        title: "{{!empty($title)?$title:'Drag Dan Tentukan Posisi Titik'}}"
      });
  
      if (jQuery('#pac-input').length > 0) {
          var input = document.getElementById('pac-input');
          var autocomplete = new google.maps.places.Autocomplete(input);
          google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            jQuery('#lat').val(place.geometry.location.lat());
            jQuery('#lng').val(place.geometry.location.lng());
            marker.setPosition(place.geometry.location);
            map.setCenter(place.geometry.location);
            map.setZoom(15);
          });
      }
      google.maps.event.addListener(marker, 'dragend', function (event) {
          jQuery('#lat').val(event.latLng.lat());
          jQuery('#lng').val(event.latLng.lng());
      });
  
      map.addListener('click', function(event) {
        cur_pos = {
          lat: event.latLng.lat(),
          lng: event.latLng.lng()
        };
  
        $("#lat").val(event.latLng.lat());
        $("#lng").val(event.latLng.lng());
  
        marker.setPosition(cur_pos);
      });
  
      if (navigator.geolocation && (!$("#lat").val() || !$("#lng").val())) {
        navigator.geolocation.getCurrentPosition(function(position) {
          cur_pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
  
          $("#lat").val(position.coords.latitude);
          $("#lng").val(position.coords.longitude);
  
          map.setCenter(cur_pos);
          marker.setPosition(cur_pos);
        });
      }
    }
    // google.maps.event.addDomListener(window, 'load', initialize);
  </script>

@endsection