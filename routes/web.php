<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('profil','ProfileController@index')->name('profil');
Route::get('profil/{page}','ProfileController@index');
Route::get('profil/detail/{id}','ProfileController@detail');

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('home', 'HomeController@index')->name('home');

    Route::resource('user', 'DataController');
    Route::get('user/delete/{id}', 'DataController@delete');
    Route::get('image/delete/{id}', 'DataController@deleteImage');
    Route::get('file/delete/{id}', 'DataController@deleteFile');
    Route::get('user_getData','DataController@getData')->name('user_getData');

    Route::resource('admin', 'AdminController');
    Route::get('admin/delete/{id}', 'AdminController@delete');
    Route::get('admin_getData','AdminController@getData')->name('admin_getData');

    Route::resource('setting', 'SettingController');
    Route::get('setting/delete/{id}', 'SettingController@delete');
    Route::get('setting_getData','SettingController@getData')->name('setting_getData');
    Route::post('setting/toggleActive','SettingController@toggleActive')->name('setting_toggleActive');

});
