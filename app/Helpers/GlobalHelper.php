<?php
namespace App\Helpers;

use App\Setting;

class GlobalHelper{
    public static function getSetting($key){
        $data = Setting::where('key', $key)->first();
        if($data != null){
            if($data->status == 1){
                if($data->type == 'file')
                    return 'storage/'.$data->value;
                else
                    return $data->value;
            }
            else
                return '';
        }
        else
            return '';
    }
}