@extends('admin.layouts.app', ['active' => 'Pengaturan'])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Pengaturan</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('setting.index') }}">Pengaturan</a></li>
            <li class="breadcrumb-item active">Edit</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="nav-icon fas fa-cog"></i> Edit Pengaturan</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('setting.update', $data->id) }}" method="post" class="form-horizontal" id="formData" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="key" class="col-sm-2 col-form-label">Key</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Key" autocomplete="off" value="{{ $data->key }}" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="value" class="col-sm-2 col-form-label">Tipe</label>
                    <div class="col-sm-10">
                        <input type="text" disabled value="{{ $data->type }}" class="form-control">
                    </div>
                </div>
        
                @if($data->type == 'text')
                <div class="form-group row">
                    <label for="value" class="col-sm-2 col-form-label">Value</label>
                    <div class="col-sm-10">
                        <textarea name="value" id="value" cols="20" rows="5" placeholder="Value" class="form-control">{{ $data->value }}</textarea>
                    </div>
                </div>
                @else
                <div id="type_file" class="form-group row type_value">
                    <label for="value" class="col-sm-2 col-form-label">File</label>
                    <div class="col-sm-10">
                      <input type="file" name="file" id="file" class="form-control">
                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="desc" class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
                      <textarea name="desc" id="desc" cols="20" rows="5" placeholder="Deskripsi" class="form-control">{{ $data->desc }}</textarea>
                    </div>
                </div>

                <div class="float-right">
                    <button id="submit" class="btn btn-success" type="submit">Simpan</button>
                </div>                

            </form> 
        </div>    
    </div>    
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection

@section('javascripts')
<script>
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000
        });        

    $(document).ready(function(){        

        $('#showPassword').change(function(e){
            if(this.checked)
                $('#password').attr('type','text');
            else
                $('#password').attr('type','password');
        });

        $('#formData').submit(function(e){
            e.preventDefault();            
            $('#submit').attr('disabled', true);
            $('#submit').html('Menyimpan <i class="fas fa-sync-alt fa-spin"></i>');   
            var formData = new FormData($(this)[0]);
            $.ajax({
                method : $(this).attr('method'),
                url : $(this).attr('action'),
                data : formData,
                processData: false,
                contentType: false,
                success : function(data){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');
                    Swal.fire(
                        'Selesai!',
                        'Data berhasil disimpan.',
                        'success'
                    )
                    $('#password').attr('type','password');
                    $('#password').val('');
                    $('#showPassword').attr('checked',false)
                },
                error: function(err){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');                    
                    var response = JSON.parse(err.responseText);
                    
                    var errorString = '';
                    if(typeof response.message === 'object'){
                        $.each( response.message, function( key, value) {
                            errorString += value + "<br>";
                        });
                    }else{
                        errorString = response.message;
                    }
                    Swal.fire(
                        'Error!',
                        errorString,
                        'error'
                    )
                }
            })            

        });
    });
</script>
@endsection