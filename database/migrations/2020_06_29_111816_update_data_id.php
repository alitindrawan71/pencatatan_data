<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDataId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('file', function (Blueprint $table){
            $table->bigInteger('data_id')->unsigned()->after('mime_type');
            $table->foreign('data_id')->references('id')->on('data')->onDelete('cascade');
        });

        Schema::table('image', function (Blueprint $table){
            $table->bigInteger('data_id')->unsigned()->after('path');
            $table->foreign('data_id')->references('id')->on('data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('file', function (Blueprint $table){
            $table->dropForeign('file_data_id_foreign');
            // $table->dropIndex('file_data_id_index');
            $table->dropColumn('data_id');
        });

        Schema::table('image', function (Blueprint $table){
            $table->dropForeign('image_data_id_foreign');
            // $table->dropIndex('image_data_id_index');
            $table->dropColumn('data_id');
        });
    }
}
