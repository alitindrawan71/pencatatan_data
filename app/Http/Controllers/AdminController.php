<?php

namespace App\Http\Controllers;

use Validator;
use DB;

use Illuminate\Http\Request;

use App\User;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class AdminController extends Controller
{
    //
    public function index(){
        return view('admin.module.admin.index');
    }

    public function create(){
        return view('admin.module.admin.create');
    }

    public function store(Request $request){
        $messages = [
            'username.required' => 'Username tidak boleh kosong',
            'password.required' => 'Password tidak boleh kosong',
            'username.unique' => 'Username sudah terdaftar di database',
            'password.min' => 'Password minimal 8 karakter',
        ];  
        $validator = Validator::make($request->input(), array(
            "username"              => 'required|unique:users',
            "password"         => 'required|min:8',
        ), $messages);
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }

        try {
            DB::beginTransaction();

            User::create([
                'username' => $request->username,
                'email' => Carbon::parse(Carbon::now())->format('hisymd')."@gmail.com",
                'password' => bcrypt($request->password),
            ]);

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function edit($id){
        $data = User::find($id);
        return view('admin.module.admin.edit',compact('data'));
    }

    public function show($id){
        $data = User::find($id);
        return view('admin.module.admin.show',compact('data'));
    }

    public function update($id,Request $request){
        $messages = [
            'username.required' => 'Username tidak boleh kosong',
            'username.unique' => 'Username sudah terdaftar di database',
            'password.min' => 'Password minimal 8 karakter',
        ];  
        $validator = Validator::make($request->input(), array(
            "username"             => 'required|unique:users,username,'.$id,
            "password"             => 'min:8|nullable',
        ), $messages);
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }

        try {
            DB::beginTransaction();

            User::find($id)->update([
                'username' => $request->username
            ]);

            if($request->password != null){
                User::find($id)->update([
                    'password' => bcrypt($request->password),
                ]);                
            }

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function delete($id){
        try {
            User::find($id)->delete();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }   
    }

    public function getData(){
        $query = User::select('*')->latest();
        return Datatables::of($query)
        ->addIndexColumn()        
        ->addColumn('action', function($data){
            return "
                <a href=".url('admin/'.$data->id)."><button class='btn btn-sm btn-secondary'><i class='fa fa-eye'></i><span class='d-none d-sm-inline'> Lihat</span></button></a>
                <a href=".url('admin/'.$data->id.'/edit')."><button class='btn btn-sm btn-success'><i class='fa fa-edit'></i><span class='d-none d-sm-inline'> Edit</span></button></a>
                <button onclick='del(".$data->id.",`".$data->username."`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i><span class='d-none d-sm-inline'> Hapus</span></button>
            ";
        })
        ->rawColumns([
            'action',
        ])
        ->make(true);
    }
}
