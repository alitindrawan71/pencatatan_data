<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ GH::getSetting('web_name') }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('image/favicon.png') }}" rel="icon">
  <link href="{{ asset('image/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/lightbox/css/lightbox.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: BizPage - v3.0.0
  * Template URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
  * Author: <BootstrapMade class="com"></BootstrapMade>
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  
  <header id="header" class="fixed-top header-transparent">
    <div class="container-fluid">

      <div class="row justify-content-center">
        <div class="col-xl-11 d-flex align-items-center">
          <h1 class="logo mr-auto"><a href="{{ url('/') }}">{{ GH::getSetting('web_name') }}</a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="index.html" class="logo mr-auto"><img src="asset/img/logo.png" alt="" class="img-fluid"></a>-->

          <nav class="nav-menu d-none d-lg-block">
            <ul>
              <li @if($active == 'profil') class="active" @endif><a href="{{ route('profil') }}">Profil</a></li>
              <li @if($active == 'login') class="active" @endif><a href="{{ route('login') }}" >Login</a></li>
            </ul>
          </nav><!-- .nav-menu -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->

@yield('content')

<!-- ======= Footer ======= -->

@if($active == 'login')
<footer id="footer" class="footer">
    <div class="container">
      <div class="copyright" style="color: black">
        &copy; Copyright <strong>2020</strong>. All Rights Reserved
      </div>
      <div class="credits">
      </div>
    </div>
  </footer><!-- End Footer -->
@elseif($active == 'profil')
@else
<footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>2020</strong>. All Rights Reserved
      </div>
      <div class="credits">
      </div>
    </div>
  </footer><!-- End Footer -->
@endif

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- plugins JS Files -->
  <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('plugins/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('plugins/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('plugins/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('plugins/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('plugins/aos/aos.js') }}"></script>
  <script src="{{ asset('plugins/lightbox/js/lightbox.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('js/main.js') }}"></script>
@yield('javascripts')
</body>

</html>