@extends('layouts.app',['active' => 'home'])
@section('content')
  <!-- ======= Intro Section ======= -->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">        

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="background-image: url({{ GH::getSetting('intro_image') }})">
            <div class="carousel-container">
              <div class="container">
                <h2 class="animate__animated animate__fadeInDown">{{ GH::getSetting('intro_header_text') }}</h2>
                <p class="animate__animated animate__fadeInUp">{{ GH::getSetting('intro_body_text') }}</p>
              </div>
            </div>
          </div>          

        </div>        

      </div>
    </div>
  </section><!-- End Intro Section -->

  <main id="main">    

    <!-- ======= About Us Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h3>Profil User</h3>
          <p>{{ GH::getSetting('profil_user_desc') }}</p>
        </header>
        <div class="row about-cols">   
          @foreach (App\Data::limit(3)->latest()->get() as $item)                  
            
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                <a href="{{ url('profil/detail/'.$item->id) }}">
                <div class="about-col">
                  <div class="img">
                    <img src="{{ $item->profile->path != null ? url("storage/".$item->profile->path) : asset('image/default.png') }}" height="400" width="400" alt="" style="height: 250px;
                    max-width: 100%;
                    object-fit: cover;">
                  </div>
                  <h2 class="title">{{ $item->nama }}</h2>
                  <p></p>
                </div>
                </a>
              </div>            
          
          @endforeach
        </div>

        <div class="row">
        	<div id="loadMore" class="container text-center">
        		<a href="{{ url('profil') }}" class="btn-get-started scrollto animate__animated animate__fadeInUp">Lihat Lainnya</a>
        	</div>
        </div>        

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= Services Section ======= -->
    <section id="services">
      <div class="container" data-aos="fade-up">

        <header class="section-header wow fadeInUp">
          <h3>Tentang</h3>
          <p>{{ GH::getSetting('about_desc') }}</p>
        </header>

        <!-- <div class="row">

          <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a href="">Lorem Ipsum</a></h4>
            <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
          </div>
          <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a href="">Dolor Sitema</a></h4>
            <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
          </div>
          <div class="col-lg-4 col-md-6 box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
            <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>          

        </div> -->

      </div>
    </section><!-- End Services Section -->    

  </main><!-- End #main -->  
@endsection