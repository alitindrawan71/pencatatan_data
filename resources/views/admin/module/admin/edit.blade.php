@extends('admin.layouts.app', ['active' => 'Admin'])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Admin</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Admin</a></li>
            <li class="breadcrumb-item active">Edit</a></li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-user"></i> Edit Admin</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.update', $data->id) }}" method="post" class="form-horizontal" id="formData" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="username" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off" value="{{ $data->username }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password Baru</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off" value="">
                      <small class="form-text text-muted"><i class="fas fa-exclamation-circle"></i>&nbsp;Password minimal terdiri dari 8 karakter. (Kosongkan jika tidak mengganti password)</small>
                      <input id="showPassword" type="checkbox"> <label for="showPassword">Lihat Password</label>
                    </div>                    
                </div>      

                <div class="float-right">
                    <button id="submit" class="btn btn-success" type="submit">Simpan</button>
                </div>                

            </form> 
        </div>    
    </div>    
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection

@section('javascripts')
<script>
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000
        });        

    $(document).ready(function(){        

        $('#showPassword').change(function(e){
            if(this.checked)
                $('#password').attr('type','text');
            else
                $('#password').attr('type','password');
        });

        $('#formData').submit(function(e){
            e.preventDefault();            
            $('#submit').attr('disabled', true);
            $('#submit').html('Menyimpan <i class="fas fa-sync-alt fa-spin"></i>');   
            var formData = new FormData($(this)[0]);
            $.ajax({
                method : $(this).attr('method'),
                url : $(this).attr('action'),
                data : formData,
                processData: false,
                contentType: false,
                success : function(data){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');
                    Swal.fire(
                        'Selesai!',
                        'Data berhasil disimpan.',
                        'success'
                    )
                    $('#password').attr('type','password');
                    $('#password').val('');
                    $('#showPassword').attr('checked',false)
                },
                error: function(err){
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Simpan');                    
                    var response = JSON.parse(err.responseText);
                    
                    var errorString = '';
                    if(typeof response.message === 'object'){
                        $.each( response.message, function( key, value) {
                            errorString += value + "<br>";
                        });
                    }else{
                        errorString = response.message;
                    }
                    Swal.fire(
                        'Error!',
                        errorString,
                        'error'
                    )
                }
            })            

        });
    });
</script>
@endsection