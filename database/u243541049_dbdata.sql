-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 03 Agu 2020 pada 02.20
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u243541049_dbdata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE `data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`id`, `nama`, `jenis_kelamin`, `email`, `no_telepon`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'I Gusti Ngurah Alit Indrawan', 'L', 'alitindrawan71@gmail.com', '089602641324', 'Bali', '2020-07-01', NULL, NULL, NULL, '2020-07-08 07:13:45', '2020-07-21 15:08:59'),
(2, 'I GUSTI BAGUS HADI WIDHINUGRAHA', 'L', 'balance_clothink@yahoo.com', '087882627817', 'Badung', '2020-07-11', NULL, NULL, NULL, '2020-07-10 01:17:46', '2020-07-10 01:17:46'),
(3, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(4, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(5, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(6, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(7, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(8, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(9, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(10, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(11, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(12, 'Dummy', 'L', 'dummy@gmail.com', '089', '-', '2020-07-17', NULL, NULL, NULL, '2020-07-17 05:18:32', '2020-07-17 05:18:35'),
(13, 'Test', 'L', 'test@gmail.com', '089', 'Bali', '2020-07-29', NULL, '-8.657226704105948', '115.21757125854492', '2020-07-19 04:06:25', '2020-07-19 04:38:51'),
(14, 'Test 2', 'L', 'test24@gmail.com', '089', 'Bali', '2020-07-09', 'Jalan Malioboro, Kembang, Sosromenduran, Kota Yogyakarta, Daerah Istimewa Yogyakarta, Indonesia', '-7.792630600000001', '110.3658442', '2020-07-19 11:16:14', '2020-07-26 18:13:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `file`
--

CREATE TABLE `file` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `file`
--

INSERT INTO `file` (`id`, `name`, `path`, `size`, `mime_type`, `data_id`, `created_at`, `updated_at`) VALUES
(1, '0305527.pdf', 'user/file/3cce3e9ff13218645a0edcdc6fe32a05_unique_5f057229e75a8.pdf', '205498', 'pdf', 1, '2020-07-08 07:13:46', '2020-07-08 07:13:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `image`
--

CREATE TABLE `image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_profile` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `image`
--

INSERT INTO `image` (`id`, `name`, `path`, `data_id`, `created_at`, `updated_at`, `is_profile`) VALUES
(1, '1592486592260.jpg', 'user/profil/91dfc010049d6ab5580c4c6848bead6e_unique_5f057229cdb81.jpg', 1, '2020-07-08 07:13:45', '2020-07-08 07:13:45', 1),
(4, 'Screenshot_2020-04-20-20-20-53-259_game.pokemonshowdownpro.jpg', 'user/tambahan/3215b3abf32171c3144f5139cfee432f_unique_5f05d240df4f2.jpg', 1, '2020-07-08 14:03:44', '2020-07-08 14:03:44', 0),
(5, 'Screenshot_2020-04-20-20-06-55-125_game.pokemonshowdownpro.jpg', 'user/tambahan/c297a12563942762a713faf2df18714e_unique_5f05d240e02c9.jpg', 1, '2020-07-08 14:03:44', '2020-07-08 14:03:44', 0),
(6, '11.jpg', 'user/profil/62bf1edb36141f114521ec4bb4175579_unique_5f07c1ba9c46f.jpg', 2, '2020-07-10 01:17:46', '2020-07-10 01:17:46', 1),
(7, '1.PNG', 'user/tambahan/f7f0bbdb094d0b83d7561fc5ec2130d7_unique_5f07c1ba9edfb.PNG', 2, '2020-07-10 01:17:46', '2020-07-10 01:17:46', 0),
(11, 'default.png', NULL, 3, NULL, NULL, 1),
(12, 'default.png', NULL, 4, NULL, NULL, 1),
(13, 'default.png', NULL, 5, NULL, NULL, 1),
(14, 'default.png', NULL, 6, NULL, NULL, 1),
(15, 'default.png', NULL, 7, NULL, NULL, 1),
(16, 'default.png', NULL, 8, NULL, NULL, 1),
(17, 'default.png', NULL, 9, NULL, NULL, 1),
(18, 'default.png', NULL, 10, NULL, NULL, 1),
(19, 'default.png', NULL, 11, NULL, NULL, 1),
(20, 'default.png', NULL, 12, NULL, NULL, 1),
(21, 'default.png', NULL, 13, NULL, NULL, 1),
(22, 'default.png', NULL, 14, '2020-07-19 11:16:14', '2020-07-19 11:16:14', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_06_29_090627_create_data', 1),
(5, '2020_06_29_090703_create_setting', 1),
(6, '2020_06_29_090744_create_image', 1),
(7, '2020_06_29_091143_create_file', 1),
(8, '2020_06_29_093243_update_users', 1),
(9, '2020_06_29_111816_update_data_id', 1),
(10, '2020_07_02_122816_update_photo_add_is_profile', 1),
(11, '2020_07_08_052652_nullable_path_image', 2),
(12, '2020_07_09_075347_add_desc_setting', 3),
(13, '2020_07_11_062329_update_table_setting', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id`, `key`, `type`, `desc`, `value`, `status`, `created_at`, `updated_at`) VALUES
(2, 'intro_image', 'file', 'Gambar background pada landing page', 'setting/1af8fa7450dfc1e80a384e3886a53823_unique_5f10f7b1ec7c0.jpg', 1, '2020-07-11 06:56:05', '2020-07-17 00:58:25'),
(3, 'web_name', 'text', 'Nama website', 'WEB NAME', 1, '2020-07-11 08:17:53', '2020-07-11 08:39:31'),
(4, 'intro_header_text', 'text', 'Text header pada intro landing page', 'We are professional', 1, '2020-07-11 08:29:53', '2020-07-11 08:29:53'),
(5, 'intro_body_text', 'text', 'Text pada body intro landing page', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-07-11 08:33:09', '2020-07-11 08:33:09'),
(6, 'profil_user_desc', 'text', 'Keterangan/Deskripsi pada profil user landing page', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis trud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-07-11 08:33:40', '2020-07-11 08:33:40'),
(7, 'about_desc', 'text', 'Keterangan/deskripsi pada Tentang landing page', 'Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus, ad pro quaestio laboramus. Ei ubique vivendum pro. At ius nisl accusam lorenta zanos paradigno tridexa panatarel.', 1, '2020-07-11 08:34:34', '2020-07-11 08:34:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '2020-07-08 00:29:02', '$2y$10$DaKUgn/CP96SlfVVHjxSN.Rp1uk3DA1lKt2TP5kUMnW3RAWa6w6q6', NULL, '2020-07-08 00:29:02', '2020-07-08 00:29:02');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_data_id_foreign` (`data_id`);

--
-- Indeks untuk tabel `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_data_id_foreign` (`data_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data`
--
ALTER TABLE `data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `file`
--
ALTER TABLE `file`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `setting`
--
ALTER TABLE `setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_data_id_foreign` FOREIGN KEY (`data_id`) REFERENCES `data` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_data_id_foreign` FOREIGN KEY (`data_id`) REFERENCES `data` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
